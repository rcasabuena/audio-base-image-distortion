const demo1 = p => {
  p.demoShader.setUniform('u_resolution', [p.windowWidth, p.windowHeight]);
  p.demoShader.setUniform('u_texture', p.img);
  p.demoShader.setUniform('u_tResolution', [p.img.width, p.img.height]);
  return p;
}

const demo2 = p => {
  p.demoShader.setUniform('u_resolution', [p.windowWidth, p.windowHeight]);
  p.demoShader.setUniform('d_map', p.d_map);
  p.demoShader.setUniform('img', p.img);
  p.demoShader.setUniform('texRes', [p.img.width, p.img.height]);
  return p;
}

const demo3 = p => {
  p.demoShader.setUniform('u_resolution', [p.windowWidth, p.windowHeight]);
  p.demoShader.setUniform('u_texture', p.img);
  p.demoShader.setUniform('u_tResolution', [p.img.width, p.img.height]);
  return p;
}

const demo4 = p => {
  p.demoShader.setUniform('u_resolution', [p.windowWidth, p.windowHeight]);
  p.demoShader.setUniform('d_map', p.d_map);
  p.demoShader.setUniform('img', p.img);
  p.demoShader.setUniform('u_tResolution', [p.img.width, p.img.height]);
  return p;
}

const demo5 = p => {
  p.demoShader.setUniform('u_resolution', [p.windowWidth, p.windowHeight]);
  p.demoShader.setUniform('u_texture', p.img);
  p.demoShader.setUniform('u_tResolution', [p.img.width, p.img.height]);
  return p;
}

const demo6 = p => {
  p.demoShader.setUniform('u_resolution', [p.windowWidth, p.windowHeight]);
  p.demoShader.setUniform('u_texture', p.img);
  p.demoShader.setUniform('u_tResolution', [p.img.width, p.img.height]);
  return p;
}

const demo7 = p => {
  p.demoShader.setUniform('u_resolution', [p.windowWidth, p.windowHeight])
  p.demoShader.setUniform('u_texture', p.img)
  p.demoShader.setUniform('u_tResolution', [p.img.width, p.img.height])
  return p;
}

const demo8 = p => {
  p.demoShader.setUniform('u_resolution', [p.windowWidth, p.windowHeight]);
  p.demoShader.setUniform('d_map', p.d_map)
  p.demoShader.setUniform('u_texture', p.img)
  p.demoShader.setUniform('u_tResolution', [p.img.width, p.img.height])
  return p;
}

export const shaders = {
  demo1,
  demo2,
  demo3,
  demo4,
  demo5,
  demo6,
  demo7,
  demo8,
};
