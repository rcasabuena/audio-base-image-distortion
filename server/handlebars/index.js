import path from 'path';
import hbs from 'express-handlebars';
import { ifeq } from './hbs_helpers'

export default (app) => {

  app.set('view engine', 'hbs');

  app.engine('hbs', hbs({
    extname: 'hbs',
    defaultView: 'demo1',
    helpers: {
      ifeq: ifeq
    },
    layoutsDir: path.join(__dirname, './views/layouts'),
    partialsDir: path.join(__dirname, './views/partials'),
  }));

  app.set('views', path.join(__dirname, './views'));

  return app;
};