import { Router } from 'express';
import { getDemos, getDemoSafely } from './demos';

const router = Router();

export default () => {

  router.get('/:demo?', (req, res) => {
    const demo = getDemoSafely(req.params.demo);
    const demos = getDemos(demo);

    return res.render(demo.template, {
      layout: 'default',
      ...demo,
      demos
    });

  });

  return router;
};
