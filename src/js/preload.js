const demo1 = p => {
  p.audio = p.loadSound('audio/demo1.mp3');
  p.demoShader = p.loadShader('shaders/base.vert', 'shaders/d1.frag');
  p.img = p.loadImage('img/1.jpg');
  return p;
}

const demo2 = p => {
  p.audio = p.loadSound('audio/demo2.mp3');
  p.demoShader = p.loadShader('shaders/base.vert', 'shaders/d2.frag');
  p.img = p.loadImage('img/2.jpg');
  p.d_map = p.loadImage('img/clouds.jpg');
  return p;
}

const demo3 = p => {
  p.audio = p.loadSound('audio/demo3.mp3');
  p.demoShader = p.loadShader('shaders/base.vert', 'shaders/d3.frag');
  p.img = p.loadImage('img/3.jpg');
  return p;
}

const demo4 = p => {
  p.audio = p.loadSound('audio/demo4.mp3');
  p.demoShader = p.loadShader('shaders/base.vert', 'shaders/d4.frag');
  p.img = p.loadImage('img/4.jpg');
  p.d_map = p.loadImage('img/clouds.jpg');
  return p;
}

const demo5 = p => {
  p.audio = p.loadSound('audio/demo5.mp3');
  p.demoShader = p.loadShader('shaders/base.vert', 'shaders/d5.frag');
  p.img = p.loadImage('img/5.jpg');
  return p;
}

const demo6 = p => {
  p.audio = p.loadSound('audio/demo6.mp3');
  p.demoShader = p.loadShader('shaders/base.vert', 'shaders/d6.frag');
  p.img = p.loadImage('img/6.jpg');
  return p;
}

const demo7 = p => {
  p.audio = p.loadSound('audio/demo7.mp3');
  p.demoShader = p.loadShader('shaders/base.vert', 'shaders/d3.frag');
  p.img = p.loadImage('img/7.jpg');
  return p;
}

const demo8 = p => {
  p.audio = p.loadSound('audio/demo8.mp3');
  p.demoShader = p.loadShader('shaders/base.vert', 'shaders/d8.frag');
  p.img = p.loadImage('img/8.jpg');
  p.d_map = p.loadImage('img/clouds.jpg');
  return p;
}

export const preloaders = {
  demo1,
  demo2,
  demo3,
  demo4,
  demo5,
  demo6,
  demo7,
  demo8,
};
