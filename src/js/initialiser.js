export const initialiser = (demo, funcs) => (...args) => {

  const { demo1, demo2, demo3, demo4, demo5, demo6, demo7, demo8 } = funcs;

  switch (demo) {
    case 'demo1': 
      return demo1(...args);
      break;
    
    case 'demo2': 
      return demo2(...args);
      break;
    
    case 'demo3': 
      return demo3(...args);
      break;
    
    case 'demo4': 
      return demo4(...args);
      break;
    
    case 'demo5': 
      return demo5(...args);
      break;

    case 'demo6': 
      return demo6(...args);
      break;

    case 'demo7': 
      return demo7(...args);
      break;

    case 'demo8': 
      return demo8(...args);
      break;
    
    default: 
      return demo1(...args);
      break;
  }
};