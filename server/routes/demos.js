import _ from 'lodash';

export const demos = {
  demo1: {
    demo: 1,
    title: 'Demo 1',
    template: 'demo1',
  },
  demo2: {
    demo: 2,
    title: 'Demo 2',
    template: 'demo2',
  },
  demo3: {
    demo: 3,
    title: 'Demo 3',
    template: 'demo3',
  },
  demo4: {
    demo: 4,
    title: 'Demo 4',
    template: 'demo4',
  },
  demo5: {
    demo: 5,
    title: 'Demo 5',
    template: 'demo5',
  },
  demo6: {
    demo: 6,
    title: 'Demo 6',
    template: 'demo6',
  },
  demo7: {
    demo: 7,
    title: 'Demo 7',
    template: 'demo7',
  },
  demo8: {
    demo: 8,
    title: 'Demo 8',
    template: 'demo8',
  },
};

export const getDemo = key => demos[key];

export const getValidDemo = func => 
  (...args) => {
    if (undefined === args[0] || undefined === func(args[0])) {
      return func('demo1');
    }
    return func(...args);
  }

export const getDemoSafely = getValidDemo(getDemo);
export const getDemos = ({ demo }) => {
  return _.map(demos, el => {
    el.isActive = el.demo == demo ? true : false;
    return el;
  });
}
