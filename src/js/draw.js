const demo1 = p => {

  const bass = p.fft.getEnergy("bass");
  const treble = p.fft.getEnergy("treble");
  const mid = p.fft.getEnergy("mid");

  const mapBass = p.map(bass, 0, 255, 10, 15.0);
  const mapTremble = p.map(treble, 0, 255, 0, 0.0);
  const mapMid = p.map(mid, 0, 255, 0.0, 0.1);

  p.demoShader.setUniform('u_time', p.frameCount / 20);
  p.demoShader.setUniform('u_bass', mapBass);
  p.demoShader.setUniform('u_tremble', mapTremble);
  p.demoShader.setUniform('u_mid', mapMid);

  return p;
}

const demo2 = p => {

  p.background(0);

  const bass = p.fft.getEnergy("bass");
  const mid = p.fft.getEnergy("highMid");
  const lowMid = p.fft.getEnergy("mid");

  const mapBass = p.map(bass, 0, 255, 0.0, 0.04)
  const mapMid  = p.map(mid, 0, 30, 0.0, 0.8)
  const mapLowMid  = p.map(lowMid, 0, 60, 0.0, 0.4)

  const tc = p.map(p.audio.currentTime(), 0, p.audio.duration(), 1.0, 1.0)
  p.demoShader.setUniform('u_time', tc)
  p.demoShader.setUniform('u_bass', mapBass)
  p.demoShader.setUniform('u_mid', mapMid)
  p.demoShader.setUniform('u_lowmid', mapLowMid)

  return p;
}

const demo3 = p => {

  const bass = p.fft.getEnergy("bass");
  const treble = p.fft.getEnergy("treble");
  const mid = p.fft.getEnergy("mid");

  const mapBass = p.map(bass, 0, 255, 0, 15.0);
  const mapTremble = p.map(treble, 0, 255, 0, 0.0);
  const mapMid = p.map(mid, 0, 255, 0.0, 0.2);

  p.demoShader.setUniform('u_time', p.frameCount / 20);
  p.demoShader.setUniform('u_bass', mapBass);
  p.demoShader.setUniform('u_tremble', mapTremble);
  p.demoShader.setUniform('u_mid', mapMid);

  return p;
}

const demo4 = p => {

  p.background(0);

  const bass = p.fft.getEnergy("bass");
  const mid = p.fft.getEnergy("mid");

  const mapBass = p.map(bass, 0, 255, 0, 0.02);
  const mapMid = p.map(mid, 0, 70, 0, 10.001);

  const tc = p.map(p.audio.currentTime(), 0, p.audio.duration(), 2.0, 2.0);
  p.demoShader.setUniform('u_time', tc)
  p.demoShader.setUniform('u_bass', mapBass)
  p.demoShader.setUniform('u_mid', mapMid)

  return p;
}

const demo5 = p => {

  const bass = p.fft.getEnergy("bass");
  const treble = p.fft.getEnergy("treble");
  const mid = p.fft.getEnergy("mid");

  const mapBass = p.map(bass, 0, 150, 0, 13.0);
  const mapTremble = p.map(treble, 0, 255, 0, 0.5);
  const mapMid = p.map(mid, 0, 255, 0.0, 0.1);

  p.demoShader.setUniform('u_time', p.frameCount / 8);
  p.demoShader.setUniform('u_bass', mapBass);
  p.demoShader.setUniform('u_tremble', mapTremble);
  p.demoShader.setUniform('u_mid', mapMid);

  return p;
}

const demo6 = p => {

  const bass = p.fft.getEnergy("bass");
  const treble = p.fft.getEnergy("treble");
  const mid = p.fft.getEnergy("mid");

  const mapBass = p.map(bass, 0, 255, 10, 15.0);
  const mapTremble = p.map(treble, 0, 255, 0, 0.0);
  const mapMid = p.map(mid, 0, 255, 0.0, 0.1);

  p.demoShader.setUniform('u_time', p.frameCount / 20);
  p.demoShader.setUniform('u_bass', mapBass);
  p.demoShader.setUniform('u_tremble', mapTremble);
  p.demoShader.setUniform('u_mid', mapMid);

  return p;
}

const demo7 = p => {

  const bass = p.fft.getEnergy("bass");
  const mid = p.fft.getEnergy("mid");

  const mapBass = p.map(bass, 0, 255, 0.0, 2.0);
  const mapMid = p.map(mid, 0, 255, 0.0, 0.05);

  p.demoShader.setUniform('u_time', p.frameCount / 20.0);
  p.demoShader.setUniform('u_bass', mapBass);
  p.demoShader.setUniform('u_mid', mapMid);

  return p;
}

const demo8 = p => {

  const bass = p.fft.getEnergy("bass");
  const treble = p.fft.getEnergy("treble");
  const mid = p.fft.getEnergy("mid");

  const mapBass     = p.map(bass, 0, 255, 5, 10.0);
  const mapTremble  = p.map(treble, 0, 255, 0, 0.0);
  const mapMid      = p.map(mid, 0, 255, 0.0, 0.1);

  p.demoShader.setUniform('u_time', p.frameCount / 20);
  p.demoShader.setUniform('u_bass', mapBass);
  p.demoShader.setUniform('u_tremble', mapTremble);
  p.demoShader.setUniform('u_mid', mapMid);

  return p;
}

export const drawers = {
  demo1,
  demo2,
  demo3,
  demo4,
  demo5,
  demo6,
  demo7,
  demo8,
};
