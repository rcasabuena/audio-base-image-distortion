export function ifeq(a, b, options) {
    if (a == b) { return options.fn(this) }
    return options.inverse(this)
}

export function ifnoteq(a, b, options) {
    if (a != b) { return options.fn(this) }
    return options.inverse(this)
}

