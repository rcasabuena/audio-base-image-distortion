import '../sass/styles.scss';
import { initialiser } from './initialiser';
import { preloaders } from './preload';
import { shaders } from './setupShader';
import { drawers } from './draw';

const s = (p) => {
  let demo = document.getElementsByTagName('body')[0].classList[0];
  demo = undefined == demo ? 'demo1' : demo;

  p.preload = () => {
    p = initialiser(demo, preloaders)(p);
  }

  p.setup = () => {
      const playBtn = document.querySelector('#play-btn');
      playBtn.addEventListener('click', () => {
        document.body.classList.add('start-anim')
          p.audio.loop()
      })

      p.pixelDensity(1)
      p.createCanvas(p.windowWidth, p.windowHeight, p.WEBGL)

      const toggleBtn = document.querySelector('#toggle-btn')
      toggleBtn.addEventListener('click', () => {
        toggleBtn.classList.toggle('toggle--on')
        p.toggleAudio()
      })

      p.fft = new p5.FFT()
      p.shader(p.demoShader)
      p = initialiser(demo, shaders)(p);
  }

  p.draw = () => {
    p.fft.analyze()
    p = initialiser(demo, drawers)(p);
    p.rect(0,0, p.width, p.height)
  }

  p.windowResized = () => {
    p.resizeCanvas(p.windowWidth, p.windowHeight)
    p.demoShader.setUniform('u_resolution', [p.windowWidth, p.windowHeight])
  }

  p.toggleAudio = () => {
    if (p.audio.isPlaying()) {
      p.audio.pause()
    } else {
      p.audio.loop()
    }
  }
};

new p5(s)
