import express from 'express';
import hbs from './handlebars';
import routes from './routes';
import path from 'path';

const app = express();

hbs(app);

app.use(express.static(path.join(__dirname, '../dist')));
app.use('/', routes());

const port = 8080;

app.listen(port, () => {
  console.log(`Express server listening to port ${port}!`);
});
